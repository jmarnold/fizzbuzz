# FizzBuzz

## Decisions

* I'm using a plain/vanilla NuGet solution for the repository (with package restore enabled). My preferred build automation is done through rake and a handful of Fubu tooling but that seems like overkill.
* I'm just outputting information rather than prompting for ranges through the Console


## Interesting parts

* Test coverage should illustrate all usages of the library
* Configuration is the key component (defining range, output writer, tokens, etc.)