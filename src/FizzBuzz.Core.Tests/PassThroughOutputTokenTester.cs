﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class PassThroughOutputTokenTester
    {
        private PassThroughOutputToken theToken;

        [SetUp]
        public void SetUp()
        {
            theToken = new PassThroughOutputToken();
        }

        [Test]
        public void always_matches()
        {
            theToken.Matches(-35).ShouldBeTrue();
            theToken.Matches(1).ShouldBeTrue();
            theToken.Matches(1000).ShouldBeTrue();
        }

        [Test]
        public void just_tostrings_the_integer()
        {
            theToken.CreateToken(1234).ShouldEqual("1234");
        }
    }
}