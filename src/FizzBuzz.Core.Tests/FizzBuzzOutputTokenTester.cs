﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class FizzBuzzOutputTokenTester
    {
        public const string OUTPUT = "74k,,,,,";

        private FizzBuzzOutputToken theToken = new FizzBuzzOutputToken(OUTPUT);

        [Test]
        public void matches_numbers_divisible_by_three_and_five()
        {
            theToken.Matches(15).ShouldEqual(true);
            theToken.Matches(45).ShouldEqual(true);
        }

        [Test]
        public void matches_numbers_divisible_by_three_negative()
        {
            theToken.Matches(9).ShouldEqual(false);
            theToken.Matches(5).ShouldEqual(false);
        }

        [Test]
        public void returns_the_input()
        {
            theToken.CreateToken(1).ShouldEqual(OUTPUT);
        }
    }
}