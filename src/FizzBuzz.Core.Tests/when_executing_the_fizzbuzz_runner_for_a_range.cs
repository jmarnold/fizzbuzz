﻿using FubuTestingSupport;
using NUnit.Framework;
using Rhino.Mocks;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class when_executing_the_fizzbuzz_runner_for_a_range : InteractionContext<FizzBuzzRunner>
    {
        protected override void beforeEach()
        {
            Services.Inject(new Range(-32, 100));
            Services.PartialMockTheClassUnderTest();

            ClassUnderTest
                .Stub(x => x.SelectToken(0))
                .IgnoreArguments()
                .Return(new PassThroughOutputToken());

            ClassUnderTest.Execute();
        }

        [Test]
        public void writes_output_the_correct_number_of_times()
        {
            MockFor<IOutputWriter>().AssertWasCalled(x => x.Write(""), o =>
            {
                o.IgnoreArguments();
                o.Repeat.Times(133);
            });
        }
    }
}