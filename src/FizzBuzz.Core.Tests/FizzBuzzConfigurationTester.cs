﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class FizzBuzzConfigurationTester
    {
        private FizzBuzzConfiguration theConfiguration;

        [SetUp]
        public void SetUp()
        {
            theConfiguration = new FizzBuzzConfiguration { Start = 1, End = 1000 };
        }

        [Test]
        public void gets_the_range()
        {
            theConfiguration.Range().ShouldEqual(new Range(1, 1000));
        }

        [Test]
        public void adds_tokens()
        {
            theConfiguration.AddOutputToken(new PassThroughOutputToken());
            theConfiguration.AllTokens().ShouldHaveTheSameElementsAs(new PassThroughOutputToken());
        }
    }
}