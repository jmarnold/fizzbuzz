﻿using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class IntegratedFizzBuzzRunnerTester
    {
        [Test]
        public void simple_scenario()
        {
            FizzBuzzScenario
                .For(1, 15)
                .Fizz(4)
                .Buzz(2)
                .FizzBuzz(1);
        }

        [Test]
        public void larger_scenario()
        {
            var counts = FizzBuzzScenario.For(-5, 30);

            FizzBuzzScenario
                .For(-5, 30)
                .Fizz(9)
                .Buzz(5)
                .FizzBuzz(2);
        }
    }
}