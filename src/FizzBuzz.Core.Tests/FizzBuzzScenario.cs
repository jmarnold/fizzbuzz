using FubuCore.Util;
using FubuTestingSupport;

namespace FizzBuzz.Core.Tests
{
    public class FizzBuzzScenario
    {
        public static FizzBuzzScenario For(int start, int end)
        {
            var theWriter = new RecordingOutputWriter();

            var config = FizzBuzzConfiguration.Basic(start, end);
            config.Writer = theWriter;

            var runner = FizzBuzzRunner.FromConfiguration(config);
            runner.Execute();

            return new FizzBuzzScenario(theWriter.Counts);
        }

        private readonly Cache<string, int> _counts;

        public FizzBuzzScenario(Cache<string, int> counts)
        {
            _counts = counts;
        }

        public void AssertCount(string token, int count)
        {
            _counts[token].ShouldEqual(count);
        }

        public FizzBuzzScenario Fizz(int count)
        {
            AssertCount(FizzOutputToken.Fizz, count);
            return this;
        }

        public FizzBuzzScenario Buzz(int count)
        {
            AssertCount(BuzzOutputToken.Buzz, count);
            return this;
        }

        public FizzBuzzScenario FizzBuzz(int count)
        {
            AssertCount(FizzBuzzOutputToken.FizzBuzz, count);
            return this;
        }
    }
}