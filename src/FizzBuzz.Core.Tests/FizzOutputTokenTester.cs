﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class FizzOutputTokenTester
    {
        public const string SOMETHING = "112345";

        private FizzOutputToken theToken = new FizzOutputToken(SOMETHING);

        [Test]
        public void matches_numbers_divisible_by_three()
        {
            theToken.Matches(3).ShouldEqual(true);
            theToken.Matches(9).ShouldEqual(true);
            theToken.Matches(27).ShouldEqual(true);
        }

        [Test]
        public void matches_numbers_divisible_by_three_negative()
        {
            theToken.Matches(5).ShouldEqual(false);
        }

        [Test]
        public void returns_the_input()
        {
            theToken.CreateToken(1).ShouldEqual(SOMETHING);
        }
    }
}