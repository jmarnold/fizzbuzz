using FubuCore.Util;

namespace FizzBuzz.Core.Tests
{
    public class RecordingOutputWriter : IOutputWriter
    {
        public readonly Cache<string, int> Counts = new Cache<string, int>(x => 0);

        public void Write(string token)
        {
            Counts[token]++;
        }
    }
}