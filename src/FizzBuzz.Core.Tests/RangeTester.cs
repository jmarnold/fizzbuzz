﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class RangeTester
    {
        [Test]
        public void executes_the_continuation_for_each_value()
        {
            var count = 0;
            new Range(-50, 75).Each(x => ++count);

            count.ShouldEqual(126);
        }

        [Test]
        public void equals()
        {
            new Range(32, 40).ShouldEqual(new Range(32, 40));
        }
    }
}