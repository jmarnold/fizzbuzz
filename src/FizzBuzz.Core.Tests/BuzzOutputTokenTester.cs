﻿using FubuTestingSupport;
using NUnit.Framework;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class BuzzOutputTokenTester
    {
        public const string SOMETHING_ELSE = "aheguherg";

        private BuzzOutputToken theToken = new BuzzOutputToken(SOMETHING_ELSE);

        [Test]
        public void matches_numbers_divisible_by_five()
        {
            theToken.Matches(5).ShouldEqual(true);
            theToken.Matches(35).ShouldEqual(true);
            theToken.Matches(125).ShouldEqual(true);
        }

        [Test]
        public void matches_numbers_divisible_by_five_negative()
        {
            theToken.Matches(22).ShouldEqual(false);
        }

        [Test]
        public void returns_the_input()
        {
            theToken.CreateToken(1).ShouldEqual(SOMETHING_ELSE);
        }
    }
}