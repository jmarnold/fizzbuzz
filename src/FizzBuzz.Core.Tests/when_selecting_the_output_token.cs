﻿using FubuTestingSupport;
using NUnit.Framework;
using Rhino.Mocks;

namespace FizzBuzz.Core.Tests
{
    [TestFixture]
    public class when_selecting_the_output_token
    {
        private IOutputToken t1;
        private IOutputToken t2;
        private Range theRange;
        private FizzBuzzRunner theRunner;

        [SetUp]
        public void SetUp()
        {
            t1 = MockRepository.GenerateStub<IOutputToken>();
            t2 = MockRepository.GenerateStub<IOutputToken>();

            theRange = new Range(1, 1);

            var theWriter = MockRepository.GenerateStub<IOutputWriter>();

            theRunner = new FizzBuzzRunner(theRange, theWriter, new[] { t1, t2 } );
        }

        [Test]
        public void first_token_matches()
        {
            t1.Stub(x => x.Matches(1)).Return(true);

            theRunner.SelectToken(1).ShouldEqual(t1);
        }

        [Test]
        public void second_token_matches()
        {
            t1.Stub(x => x.Matches(1)).Return(false);
            t2.Stub(x => x.Matches(1)).Return(true);

            theRunner.SelectToken(1).ShouldEqual(t2);
        }

        [Test]
        public void selects_the_last()
        {
            t1.Stub(x => x.Matches(1)).Return(true);
            t2.Stub(x => x.Matches(1)).Return(true);

            theRunner.SelectToken(1).ShouldEqual(t2);
        }

        [Test]
        public void no_matching_token_uses_passthrough()
        {
            t1.Stub(x => x.Matches(1)).Return(false);
            t2.Stub(x => x.Matches(1)).Return(false);

            theRunner.SelectToken(1).ShouldBeOfType<PassThroughOutputToken>();
        }
    }
}