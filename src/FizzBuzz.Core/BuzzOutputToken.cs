﻿namespace FizzBuzz.Core
{
    public class BuzzOutputToken : OutputToken
    {
        public const string Buzz = "Buzz";

        public BuzzOutputToken()
            : this(Buzz)
        {
        }

        public BuzzOutputToken(string output)
            : base(output, value => (value % 5) == 0)
        {
        }
    }
}