﻿namespace FizzBuzz.Core
{
    public interface IOutputToken
    {
        bool Matches(int value);
        string CreateToken(int value);
    }
}