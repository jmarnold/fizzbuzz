﻿using System;

namespace FizzBuzz.Core
{
    public class ConsoleOutputWriter : IOutputWriter
    {
        public void Write(string token)
        {
            Console.WriteLine(token);
        }
    }
}