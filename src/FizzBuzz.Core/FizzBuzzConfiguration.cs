﻿using System.Collections.Generic;

namespace FizzBuzz.Core
{
    public class FizzBuzzConfiguration
    {
        private readonly List<IOutputToken> _tokens = new List<IOutputToken>();

        public int Start { get; set; }
        public int End { get; set; }
        public IOutputWriter Writer { get; set; }

        public Range Range()
        {
            return new Range(Start, End);
        }

        public void AddOutputToken(IOutputToken token)
        {
            _tokens.Add(token);
        }

        public IEnumerable<IOutputToken> AllTokens()
        {
            return _tokens.AsReadOnly();
        }

        public static FizzBuzzConfiguration Basic(int start = 1, int end = 100)
        {
            var config = new FizzBuzzConfiguration {Start = start, End = end};
            config.Writer = new ConsoleOutputWriter();
            config.AddOutputToken(new FizzOutputToken());
            config.AddOutputToken(new BuzzOutputToken());
            config.AddOutputToken(new FizzBuzzOutputToken());

            return config;
        }
    }
}