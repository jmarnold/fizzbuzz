﻿using System;

namespace FizzBuzz.Core
{
    public abstract class OutputToken : IOutputToken
    {
        private readonly string _output;
        private readonly Func<int, bool> _predicate;

        protected OutputToken(string output, Func<int, bool> predicate)
        {
            _output = output;
            _predicate = predicate;
        }

        public bool Matches(int value)
        {
            return value != 0 && _predicate(value);
        }

        public string CreateToken(int value)
        {
            return _output;
        }
    }
}