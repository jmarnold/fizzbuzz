﻿namespace FizzBuzz.Core
{
    public class FizzOutputToken : OutputToken
    {
        public const string Fizz = "Fizz";

        public FizzOutputToken()
            : this(Fizz)
        {
        }

        public FizzOutputToken(string output)
            : base(output, value => (value % 3) == 0)
        {
        }
    }
}