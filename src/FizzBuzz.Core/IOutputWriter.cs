﻿namespace FizzBuzz.Core
{
    public interface IOutputWriter
    {
        void Write(string token);
    }
}