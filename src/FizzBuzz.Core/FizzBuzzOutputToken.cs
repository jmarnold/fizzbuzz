﻿namespace FizzBuzz.Core
{
    public class FizzBuzzOutputToken : OutputToken
    {
         public const string FizzBuzz = "FizzBuzz";

        public FizzBuzzOutputToken()
            : this(FizzBuzz)
        {
        }

        public FizzBuzzOutputToken(string output)
            : base(output, value => (value % 3) == 0 && (value % 5) == 0)
        {
        }
    }
}