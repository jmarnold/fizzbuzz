﻿namespace FizzBuzz.Core
{
    public class PassThroughOutputToken : IOutputToken
    {
        public bool Matches(int value)
        {
            return true;
        }

        public string CreateToken(int value)
        {
            return value.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj.GetType() == GetType();
        }
    }
}