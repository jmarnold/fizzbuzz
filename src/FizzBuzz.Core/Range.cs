﻿using System;

namespace FizzBuzz.Core
{
    public class Range
    {
        public Range(int start, int end)
        {
            Start = start;
            End = end;
        }

        public int Start { get; private set; }
        public int End { get; private set; }

        public void Each(Action<int> action)
        {
            for (var i = Start; i <= End; i++)
            {
                action(i);
            }
        }

        public override string ToString()
        {
            return string.Format("Range: {0} to {1}", Start, End);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Range)) return false;
            return Equals((Range) obj);
        }

        public bool Equals(Range other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Start == Start && other.End == End;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Start*397) ^ End;
            }
        }
    }
}
