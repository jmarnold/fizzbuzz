﻿using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.Core
{
    public class FizzBuzzRunner
    {
        private readonly Range _range;
        private readonly IOutputWriter _writer;
        private readonly IEnumerable<IOutputToken> _tokens;

        public FizzBuzzRunner(Range range, IOutputWriter writer, IEnumerable<IOutputToken> tokens)
        {
            _range = range;
            _writer = writer;
            _tokens = tokens;
        }

        public void Execute()
        {
            _range.Each(x =>
            {
                var token = SelectToken(x);
                _writer.Write(token.CreateToken(x));
            });
        }

        // Keep this virtual for testing
        public virtual IOutputToken SelectToken(int value)
        {
            return _tokens.LastOrDefault(x => x.Matches(value)) ?? new PassThroughOutputToken();
        }

        public static FizzBuzzRunner FromConfiguration(FizzBuzzConfiguration config)
        {
            return new FizzBuzzRunner(config.Range(), config.Writer, config.AllTokens());
        }
    }
}