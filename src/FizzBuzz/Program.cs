﻿using System;
using FizzBuzz.Core;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            // The configuration is where you can set the tokens and the output writer
            var config = FizzBuzzConfiguration.Basic(-32, 150);
            var runner = FizzBuzzRunner.FromConfiguration(config);

            Console.WriteLine("Starting FizzBuzz ({0})", config.Range());
            Console.WriteLine("=====================================");

            runner.Execute();

            Console.WriteLine();
            Console.WriteLine("=====================================");
            Console.WriteLine("Completed");
            Console.ReadLine();
        }
    }
}
